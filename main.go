package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	_ "github.com/lib/pq"
)

/*
	Products (ID SERIAL PRIMARY KEY NOT NULL, ItemName Text NOT NULL,
	 ItemPrice int NOT NULL, checkout int);

	 //Separate logic

*/

//Product is the struct for this app
type Product struct {
	ID        string `json:"id,omitempty"`
	ItemName  string `json:"itemname,omitempty"`
	ItemPrice string `json:"itemprice,omitempty"`
}

var tpl *template.Template
var connStr string
var db *sql.DB

func init() {
	var err error

	tpl = template.Must(template.ParseGlob("templates/*.html"))
	connStr = "dbname=ordersapi user=ordersapi password=5555 host=localhost port=5432 sslmode=disable"
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal("***NO OPEN ****", err)
	}
	err = db.Ping()
	if err != nil {
		log.Fatal("***NO PING****", err)
	} else {
		fmt.Println("Ping successful!")
	}

}
func main() {

	http.HandleFunc("/createprocess", create)
	http.HandleFunc("/", read)
	http.HandleFunc("/updateprocess", update)
	http.HandleFunc("/deleteprocess", delete)
	// http.HandleFunc("/process", create)
	http.Handle("/assests/", http.StripPrefix("/assets", http.FileServer(http.Dir("./resources"))))
	http.ListenAndServe(":7000", nil)

}

//Read all data to template
func read(w http.ResponseWriter, r *http.Request) {

	rows, err := db.Query("SELECT * FROM products;")
	if err != nil {
		log.Fatal(err)
	}
	xp := []Product{}
	for rows.Next() {
		p := Product{}
		rows.Scan(&p.ID, &p.ItemName, &p.ItemPrice)
		xp = append(xp, p)
	}
	tpl.ExecuteTemplate(w, "index.html", xp)
}

//create Data To The Template
func create(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Redirect(w, r, "/", http.StatusOK) //status 302 in other words aka redirect
		return
	}

	//grab the variable

	Itemid := r.FormValue("itemID") //variable from form input: id
	ID, errr := strconv.Atoi(Itemid)

	if errr != nil {
		log.Println(errr)
	}

	ItemN := r.FormValue("itemName")  //variable from form input: name
	ItemP := r.FormValue("itemPrice") //variable from form input: price
	log.Println(ItemN, "  ", ItemP)   //confirming we have our form data

	//insert into products (itemname, itemprice) values ('Wireless Keyboard', 699);
	result, err := db.Exec("INSERT INTO products (id, itemname, itemprice) VALUES ($1, $2, $3);", ID, ItemN, ItemP)
	if err != nil {
		log.Fatal(err)
	}
	//result throws a float64 data so we've gotta handle it run
	n, err := result.RowsAffected()
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	log.Println("rows affected", n)

	http.Redirect(w, r, "/", http.StatusFound)
}

//UPDATE
func update(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		http.Redirect(w, r, "/", http.StatusFound) //status 303 in other words aka redirect
		return
	}

	//grab the variable

	ID := r.FormValue("ID_No") //variable from form input: name
	id, err := strconv.Atoi(ID)

	if err != nil {

	}
	Iname := r.FormValue("ItemName")   //variable from form input: name
	Iprice := r.FormValue("ItemPrice") //variable from form input: name

	log.Print("Successfully Updated!")
	log.Println("ID: ", ID)            //confirming we have our form data
	log.Println("ItemName: ", Iname)   //confirming we have our form data
	log.Println("ItemPrice: ", Iprice) //confirming we have our form data

	result, err := db.Exec("UPDATE products SET itemname =$1 , itemprice = $2 WHERE  id = $3;", Iname, Iprice, id)
	if err != nil {
		log.Fatal(err)
	}
	//result throws a float64 data so we've gotta handle it
	n, err := result.RowsAffected()
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	log.Println("rows affected", n)

	http.Redirect(w, r, "/", http.StatusFound)
}

//DELETE
func delete(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		http.Redirect(w, r, "/", http.StatusOK) //status 303 in other words aka redirect
		return
	}

	//grab the variable from the form

	ID := r.FormValue("ID_No") //variable from form input: name
	log.Println("ID: ", ID)    //confirming we have our form data

	result, err := db.Exec("DELETE FROM products  WHERE  id = $1;", ID)
	if err != nil {
		log.Fatal(err)
	}
	//result throws a float64 data so we've gotta handle it
	n, err := result.RowsAffected()
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	log.Println("rows affected", n)

	http.Redirect(w, r, "/", http.StatusFound)
}
