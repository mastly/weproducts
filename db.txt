CREATE DATABASE OrdersAPI;

CREATE TABLE Users(
    ID SERIAL PRIMARY KEY NOT NULL,
    FirstName Text NOT NULL,
    LastName Text NOT NULL,
    Email Text NOT NULL,
    Password Text NOT NULL
)

CREATE TABLE Products (
    ID SERIAL PRIMARY KEY NOT NULL,
    ItemName Text NOT NULL,
    ItemPrice number NOT NULL,
)


create user OrdersAPI with password '5555';

grant all privileges on OrdersAPI;

grant superuser on OrdersAPI;

SELECT, INSERT, UPDATE, DELETE, RULE, ALL
GRANT ALL PRIVILEGES ON DATABASE Orders to OrdersAPI;

SELECT, INSERT, UPDATE, DELETE, RULE, ALL
GRANT ALL PRIVILEGES ON DATABASE Orders to Users;





********* Actual Table ******
Products (ID SERIAL PRIMARY KEY NOT NULL, ItemName Text NOT NULL,
 ItemPrice int NOT NULL, checkout int);


INSERT INTO films (code, title, did, date_prod, kind) VALUES
    ('B6717', 'Tampopo', 110, '1985-02-10', 'Comedy'),
    ('HG120', 'The Dinner Game', 140, DEFAULT, 'Comedy');

CREATE TABLE Products (
    ID SERIAL PRIMARY KEY NOT NULL,
    ItemName Text NOT NULL,
    ItemPrice number NOT NULL,
)


create table Products (ID SERIAL PRIMARY KEY NOT NULL, ItemName Text NOT NULL, ItemPrice Text NOT NULL);
insert into Products (id, ItemName, ItemPrice) values (1,'Processor', 583), (2,'Keyboard', 200), (3,'Mouse', 150), (4,'Printer', 699), (5,'Monitor', 800);
insert into Products (id, ItemName, ItemPrice) values ('datacable', '50');