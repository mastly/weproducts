package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

//Test read function in main
func TestRead(t *testing.T) {

	t.Run("returns a product", func(t *testing.T) {

		request, _ := http.NewRequest(http.MethodGet, "/", nil)
		response := httptest.NewRecorder()
		//read: the function "endpoint/route" that I'm trying to get data from
		read(response, request)
		got := response.Body.String()
		want := "<html lang=\"en\">\n<head>\n    <meta charset=\"UTF-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n    <title>Product List</title>\n</head>\n<body>\n\n    <br><br>\n    <h1 style=\"text-align: center;\"><span font color = \"#2ab7fc\">We</span> Products</h1>\n    \n    \n\n   \n\n    <table style=\"width:100%\">\n        <tr>\n            <th>Item ID</th>\n          <th>Item Name</th>\n          <th>Item Price</th> \n        </tr>\n        \n        <tr>\n          <td style=\"text-align:center\">1</td>\n          <td style=\"text-align:center\">Processor</td>\n          <td style=\"text-align:center\">$583</td>\n        </tr>\n        \n        <tr>\n          <td style=\"text-align:center\">2</td>\n          <td style=\"text-align:center\">Keyboard</td>\n          <td style=\"text-align:center\">$200</td>\n        </tr>\n        \n        <tr>\n          <td style=\"text-align:center\">3</td>\n          <td style=\"text-align:center\">Mouse</td>\n          <td style=\"text-align:center\">$150</td>\n        </tr>\n        \n        <tr>\n          <td style=\"text-align:center\">4</td>\n          <td style=\"text-align:center\">Printer</td>\n          <td style=\"text-align:center\">$699</td>\n        </tr>\n        \n        <tr>\n          <td style=\"text-align:center\">5</td>\n          <td style=\"text-align:center\">Monitor</td>\n          <td style=\"text-align:center\">$800</td>\n        </tr>\n        \n        <tr>\n          <td style=\"text-align:center\">0</td>\n          <td style=\"text-align:center\"></td>\n          <td style=\"text-align:center\">$</td>\n        </tr>\n        \n      </table>\n      \n<br><br>\n\n\n<h1 style=\"text-align: center;\">Input Data</h1>\n\n<div style=\"position:relative; left:25%; top:2px;\">\n\n    <form action=\"createprocess\" method=\"POST\">\n      <label for=\"N\">Enter Product ID</label>\n        <input type=\"text\" name=\"itemID\" id=\"N\">  \n      \n      <label for=\"N\">Enter Product Name</label>\n        <input type=\"text\" name=\"itemName\" id=\"N\">\n        \n        <label for=\"N\">Enter Product Price</label>\n        <input type=\"text\" name=\"itemPrice\"id=\"N\">\n        \n        <input type=\"submit\">\n    </form>\n\n\n      <br><br>\n\n      <h1 style=\"position:relative; left:18%; top:2px;\">Update Data </h1>\n      \n      <form action=\"updateprocess\" method=\"POST\">\n\n        <label for=\"m\">Enter Product ID</label>\n        <input type=\"text\" name=\"ID_No\" id=\"m\"> \n\n        <label for=\"N\">Enter Product Name</label>\n        <input type=\"text\" name=\"ItemName\" id=\"N\">\n\n        <label for=\"N\">Enter Product Price</label>\n        <input type=\"text\" name=\"ItemPrice\" id=\"N\">\n        \n        <input type=\"submit\">\n\n      </form>\n\n      <br><br>\n\n      <h1 style=\"position:relative; left:18%; top:2px;\">Delete Data </h1>\n      \n      <form action=\"deleteprocess\" method=\"POST\">\n\n        <label for=\"m\">Enter Product ID</label>\n        <input type=\"text\" name=\"ID_No\" id=\"m\">\n        \n        <input type=\"submit\">\n\n      </form>\n\n\n</div>\n\n</body>\n</html>\n\n"

		if got != want {
			t.Errorf("got %q, want %q", got, want)
		}
	})

}

//Test Delete

func TestDelete(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/deleteprocess", nil)
	response := httptest.NewRecorder()

	handler := http.HandlerFunc(delete)
	handler.ServeHTTP(response, request)
	assert.Equal(t, http.StatusOK, response.Code, "Ok response is expected")
}

//Test Update
func TestUpdate(t *testing.T) {

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, "/updateprocess", nil)

	handler := http.HandlerFunc(update)
	handler.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "Ok response is expected")
}

//Test Create
// createprocess
func TestCreate(t *testing.T) {

	request, _ := http.NewRequest(http.MethodPost, "/createprocess", nil)
	response := httptest.NewRecorder()

	handler := http.HandlerFunc(create)
	handler.ServeHTTP(response, request)
	assert.Equal(t, http.StatusOK, response.Code, "OK response is expected")

}

func TestCreateMockDB(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' wasn't expected when opening a stub database connection", err)
	}
	defer db.Close()

	mock.ExpectExec("INSERT INTO products\\(a, b, c\\)").
		WithArgs("A", "B", "C").
		WillReturnResult(sqlmock.NewResult(-3, 3))

	_, err = db.Exec("INSERT INTO products(a, b, c) VALUES (?, ?, ?)", "A", "B", "C")
	if err != nil {
		t.Errorf("error '%s' was not expected, while inserting a row", err)
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
	//mocks the behavior not the connection basically... .still lots more to learn
}

func TestReadFunc(t *testing.T) {
	db, mock, err := sqlmock.New()

	if err != nil {
		t.Fatalf("There's an error in opening a stub database connection '%s'", err)
	}
	defer db.Close()
	//columns used from struct data
	columns := []string{"id", "itemname", "itemprice"}
	//expect begin
	mock.ExpectBegin()

	//expect query to fetch all products
	rows := mock.NewRows(columns) //rows to be returned by the query
	mock.ExpectQuery("SELECT (.+) FROM  products").
		WithArgs(4).
		WillReturnRows(rows)
	//ensure expectations are met

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations, %s", err)
	}

}
